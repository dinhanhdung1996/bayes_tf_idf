import csv
import math
import random


class Bayes:
    def __init__(self):
        self.ratio = 2/3
        self.data_set = []
        self.training_set = []
        self.test_set = []
        self.training_summaries = {}

    def read_csv(self, file_name):
        with open(file_name, "r") as csv_read:
            lines = csv.reader(csv_read)
            self.data_set = list(lines)
        for line in self.data_set:
            index = 0
            for i in line:
                line[index] = float(i)
                index += 1
        return self.data_set

    def separate(self):
        self.test_set = list(self.data_set)
        self.training_set = []
        run = int(self.ratio * len(self.test_set))
        for i in range(run):
            index = random.randint(0, len(self.test_set) - 1)
            self.training_set.append(self.test_set.pop(index))
        return self.training_set

    def separate_class(self):
        self.separate()
        separate = {}
        for data in self.training_set:
            if data[-1] not in separate:
                separate[data[-1]] = []
            separate[data[-1]].append(data)
        return separate

    @staticmethod
    def mean(numbers):
        sum = 0
        for i in numbers:
            sum += float(i)
        return sum/len(numbers)

    @staticmethod
    def standard_dev(numbers):
        avg = float(Bayes.mean(numbers))
        sum = 0
        for number in numbers:
            sum += math.pow(float(number) - avg, 2)
        variance = sum/(len(numbers) - 1)
        return math.sqrt(variance)

    def summarize_training(self):
        summary = self.separate_class()
        for item, data in summary.items():
            zip_data = zip(*data)
            self.training_summaries[item] = []
            for z in zip_data:
                self.training_summaries[item].append((self.mean(z), self.standard_dev(z)))
            del self.training_summaries[item][-1]
        return self.training_summaries

    @staticmethod
    def probability(x, mean, sd):
        if sd == 0:
            sd = 1/20000
        elif mean == 0:
            mean = 1/20000
        exponent = math.exp(- math.pow(x - mean, 2) / (2 * math.pow(sd, 2)))
        return exponent/(math.sqrt(2 * math.pi) * sd)

    def class_prob(self, input):
        prob_set = {}
        for item, value in self.training_summaries.items():
            prob_set[item] = 1
            pos = 0
            for element in value:
                x = input[pos]
                mean, sd = value[pos]
                pos += 1
                prob_set[item] *= self.probability(x, mean, sd)
        return prob_set

    def get_predict(self, input):
        best_value = -1
        best_name = None
        for key, data in self.class_prob(input).items():
            if best_name is None or best_value < data:
                best_value = data
                best_name = key
        return best_name

    def make_predictions(self):
        result = []
        for data in self.test_set:
            result.append(self.get_predict(data))
        return result

    def accuracy(self, predictions):
        count = 0
        for i in range(len(predictions)):
            if predictions[i] == self.test_set[i][-1]:
                count += 1
        return count/len(self.test_set) * 100

    def bayes(self, file_name=None):
        if len(self.data_set) == 0:
            if file_name is not None:
                self.read_csv(file_name)
            else:
                return
        self.summarize_training()
        return self.make_predictions()

new_bayes = Bayes()
prd = new_bayes.bayes("E:\Python\Bayes\pima-indians-diabetes.csv")
print(len(prd))
print(len(new_bayes.test_set))
print(new_bayes.accuracy(prd))
