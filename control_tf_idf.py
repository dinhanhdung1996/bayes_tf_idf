from __future__ import print_function
import tf_idf
import document
import word
import sort
import math


class ControlTfIdf:
    def __init__(self):
        self.doc_list = []
        self.word_list = []
        self.word_in = {}

    def add_doc(self, doc):
        if type(doc) is not document.Document:
            return
        if self.check_doc(doc):
            return
        self.doc_list.append(doc)
        doc.to_words()
        for a_word in doc.word_in:
            if a_word not in self.word_in:
                self.word_in[a_word] = 1
            else:
                self.word_in[a_word] += 1

    def add_to_word_list(self, a_word):
        if type(a_word) is not word.Word:
            print("not")
            return
        for ele in self.word_list:
            if a_word.string == ele.string:
                return False
        self.word_list.append(a_word)
        return True

    def clear_list(self):
        self.doc_list.clear()
        self.word_list.clear()

    def tf_idf(self, string, doc):
        if not doc.contains_word(string):
            return 0
        else:
            num = doc.word_in[string]/doc.number_of_words()
            return num * math.log(float(len(self.doc_list))/self.word_in[string])

    def check_doc(self, doc):
        if type(doc) is not document.Document:
            return False
        else:
            for a_doc in self.doc_list:
                if doc.string_data == a_doc.string_data:
                    return True
        return False

    def get_list_word(self):
        for doc in self.doc_list:
            print(doc.word_in)
            for item, value in doc.word_in.items():
                get_word = doc.find_word(item)
                if get_word is None:
                    continue
                else:
                    get_word.tf_idf = float(value/doc.number_of_words()) * \
                                      math.log(float(len(self.doc_list))/self.word_in[item])
        for doc in self.doc_list:
            doc.sort_list()
            for i in range(3):
                self.add_to_word_list(doc.word_list[i])
        return self.word_list

    def get_result(self, get_list=False):
        if not get_list:
            self.get_list_word()
        n = 0
        for a_word in self.word_list:
            for doc in self.doc_list:
                current = a_word.tf_idf
                a_word.tf_idf = self.tf_idf(a_word.string, doc)
                if a_word.tf_idf < current:
                    a_word.tf_idf = current
        quick = sort.Sort()
        quick.data_list = self.word_list
        quick.heap_sort()
        self.word_list = list(reversed(self.word_list ))
        return self.word_list[0:10000]
#
# document1 = """Python is a 2000 made-for-TV horror movie directed by Richard
# Clabaugh. The film features several cult favorite actors, including William
# Zabka of The Karate Kid fame, Wil Wheaton, Casper Van Dien, Jenny McCarthy,
# Keith Coogan, Robert Englund (best known for his role as Freddy Krueger in the
# A Nightmegtwgsare on Elm Street series of films), Dana Barron, David Bowe, and Sean
# Whalen. The film concerns a genetically engineered snake, a python, that
# escapes and unleashes itself on a small town. It includes the classic final
# girl scenario evident in films like Friday the 13th. It was filmed in Los Angeles,
#  California and Malibu, California. Python was followed by two sequels: Python
#  II (2002) and Boa vs. Python (2004), both also made-for-TV films."""
#
# document2 = """Python, from the Greek word (πύθων/πύθωνας), is a genus of
# nonvenomous pythons[2] found in Africa and Asia. Currently, 7 species are
# recognised.[2] A member of this genus, P. reticulatus, is among the longest
# snakes known."""
#
# document3 = """The Colt Python is a .357 Magnum caliber revolver formerly
# manufactured by Colt's Manufacturing Company of Hartford, Connecticut.
# It is sometimes referred to as a "Combat Magnum".[1] It was first introduced
# in 1955, the same year as Smith &amp; Wesson's M29 .44 Magnum. The now discontinued
# Colt Python targeted the premium revolver market segment. Some firearm
# collectors and writers such as Jeff Cooper, Ian V. Hogg, Chuck Hawks, Leroy
# Thompson, Renee Smeets and Martin Dougherty have described the Python as the
# finest production revolver ever made."""
#
# document4 = "C was originally developed by Dennis Ritchie between 1969 and 1973 at Bell Labs,[5] and used to re-implement the Unix operating system.[6] It has since become one of the most widely used programming languages of all time,[7][8] with C compilers from various vendors available for the majority of existing computer architectures and operating systems. C has been standardized by the American National Standards Institute (ANSI) since 1989 (see ANSI C) and subsequently by the International Organization for Standardization (ISO)."
#
# doc1 = document.Document()
# doc2 = document.Document()
# doc3 = document.Document()
# doc4 = document.Document()
# doc1.string_data = document1
# doc2.string_data = document2
# doc3.string_data = document3
# doc4.string_data = document4
# test = ControlTfIdf()
# test.add_doc(doc1)
# test.add_doc(doc2)
# test.add_doc(doc3)
# test.add_doc(doc4)
#
# for an_item in test.get_list_word():
#     print("" + an_item.string + " - " + str(an_item.tf_idf))
