import document
import math


class TfIDF:
    def __init__(self):
        self.doc_list = []

    def add_doc(self, doc):
        if type(doc) is not document.Document:
            return None
        else:
            self.doc_list.append(doc)
            doc.to_words()

    @staticmethod
    def calculate_tf(string, doc):
        if type(doc) is not document.Document:
            return None
        count = doc.count_word(string)
        num = doc.number_of_words()
        return count/num

    def calculate_idf(self, string):
        count = 0
        for doc in self.doc_list:
            if doc.contains_word(string):
                count += 1
        return math.log(float(len(self.doc_list)/count))

    def clear(self):
        self.doc_list.clear()

    def calculate_tf_idf(self, string, doc):
        if not doc.contains_word(string):
            return 0
        tf_data = self.calculate_tf(string, doc)
        idf_data = self.calculate_idf(string)
        tf_idf = tf_data * idf_data
        doc.find_word(string).tf_idf = tf_idf
        return tf_idf



