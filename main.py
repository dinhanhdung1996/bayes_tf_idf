import bayes_imp
import document
import control_tf_idf
from lxml import *
import glob
import os.path
from xml.dom import minidom
import sys
import tf_idf





def read_folder(ratio=2/3):
    files = glob.glob(my_path)
    official_word_list = []
    print(files)
    c = 0
    for file in files:
        temp_doc_list = control_tf_idf.ControlTfIdf()
        print(file)
        dom = minidom.parse(file)
        rows = dom.getElementsByTagName("row")
        count = 0
        for row in rows:
            fields = row.getElementsByTagName("field")
            doc_type = fields[0].firstChild.data
            string_data = fields[1].firstChild.data
            doc = document.Document()
            doc.string_data = string_data
            doc.type_doc = doc_type
            training_doc_list.add_doc(doc)
            count += 1
        print(count)
        c += 1
    return training_doc_list

print("Reading data: ", end=": ")
folder = "SVTT_Dataset"
path = os.path.abspath(os.path.dirname(__file__))
my_path = os.path.join(path, folder, "*.xml")
training_doc_list = control_tf_idf.ControlTfIdf()
temp_word_list = []


read_folder()
print(len(training_doc_list.doc_list))
print("Loading data: ")
official_word_list = training_doc_list.get_result()
data_list = []


def decode_doc(doc):
    data = []
    if type(doc) is not document.Document:
        return
    for word in official_word_list:
        tf_idf_data = training_doc_list.tf_idf(word.string, doc)
        data.append(tf_idf_data)
    data.append(doc.type_doc)
    data_list.append(data)

for doc in training_doc_list.doc_list:
    decode_doc(doc)

algor = bayes_imp.Bayes()
algor.data_set = data_list
print("calculating...")
prediction = algor.bayes()
print(algor.accuracy(prediction))
