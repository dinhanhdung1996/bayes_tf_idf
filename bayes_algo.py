import csv
import random
from io import TextIOWrapper
import math


def load_csv(file_name):
    data = csv.reader(open(file_name, "r"))
    data_set = list(data)
    for line in data_set:
        pos = 0
        for x in line:
            line[pos] = float(x)
            pos += 1
    return data_set


def split_data(data_set, ratio):
    copy = list(data_set)
    train = []
    num_train = int(ratio * len(copy))
    for i in range(num_train):
        pos = random.randint(0, len(copy)-1)
        train.append(copy.pop(pos))
    return [train, copy]


def separate_class(train_set):
    separate = {}
    for data in train_set:
        if data[-1] not in separate:
            separate[data[-1]] = []
        separate[data[-1]].append(data)
    return separate


def mean(numbers):
    num = len(numbers)
    sum = 0
    for i in numbers:
        sum += i
    return sum/num


def stdev(numbers):
    avg = mean(numbers)
    sum = 0
    for i in numbers:
        sum += math.pow(i - avg, 2)
    return math.sqrt(sum/(len(numbers) - 1))


def summarize(train_set):
    group = zip(*train_set)
    summarize_list = []
    for x in group:
        summarize_list.append((mean(x), stdev(x)))
    del summarize_list[-1]
    return summarize_list


def summarize_class(train_set):
    class_set = separate_class(train_set)
    s_class = {}
    for name, value in class_set.items():
        s_class[name] = summarize(value)
    return s_class


def gauss_prob(x, mean, stdev):
    exponent = math.exp(-math.pow(x-mean, 2)/(2 * math.pow(stdev, 2)))
    return exponent/(math.sqrt(2 * math.pi) * stdev)


def class_prob(summary, input_vector):
    prob = {}
    for class_name, class_value in summary.items():
        prob[class_name] = 1
        for i in range(len(summary[class_name])):
            x = input_vector[i]
            mean_i, stdev_i = class_value[i]
            prob[class_name] *= gauss_prob(x, mean_i, stdev_i)
    return prob

#
# def classes_prob(summary, input_vector):
#     prob = []
#     for data_vector in input_vector:
#         prob_class = class_prob(summary, data_vector)
#         prob.append(prob_class)

def predict(summary, input_vector):
    prob = class_prob(summary, input_vector)
    best_name = None
    best_value = -1
    for class_name, class_value in prob.items():
        if class_value > best_value or best_name is None:
            best_value = class_value
            best_name = class_name
    return best_name


def make_predictions(summary, input_vector):
    prediction = []
    for data_input in input_vector:
        prediction.append(predict(summary, data_input))
    return prediction


def get_currency(predict_vector, input_vector):
    correct = 0
    for i in range(len(input_vector)):
        if predict_vector[i] == input_vector[i][-1]:
            correct += 1
    return correct/len(input_vector) * 100






test_file = "E:\Python\Bayes\pima-indians-diabetes.csv"
train, copy = split_data(load_csv(test_file), 2/3)
print(len(train))
print(len(copy))
print(separate_class(train))
print("----------------")
print(summarize_class(train))
print("__________________________________________________________________")

prob = make_predictions(summarize_class(train), copy)
print(prob)
cur = get_currency(prob, copy)
print(cur)