import word
import sort


class Document:
    def __init__(self, string_data=None, type_doc="default"):
        self.word_list = []
        self.string_data = string_data
        self.word_in = {}
        self.type_doc = type_doc

    def to_words(self):
        self.word_list.clear()
        self.word_in.clear()
        if self.string_data is None:
            print("null data")
            return
        else:
            for string in self.string_data.split():
                string = string.lower()
                if string not in self.word_in:
                    self.word_in[string] = 1
                    new_word = word.Word(string)
                    self.word_list.append(new_word)
                else:
                    self.word_in[string] += 1

    def count_word(self, word, default=0):
        if word in self.word_in:
            return self.word_in[word]
        else:
            return default

    def number_of_words(self):
        sum_word = 0
        for item in self.word_in:
            sum_word += self.word_in[item]
        return sum_word

    def contains_word(self, a_word):
        if a_word not in self.word_in:
            return False
        return True

    def find_word(self, string):
        for data in self.word_list:
            if data.string == string:
                return data
        else:
            return None

    def sort_list(self):
        quick = sort.Sort()
        quick.data_list = self.word_list
        quick.quick_sort(0, len(self.word_list) - 1)
        self.word_list = list(reversed(self.word_list))




